# My `zsh` Config

Here is the `zsh` config I've been using for a little while. Feel free to steal it and use it too.

## Features

* Based on `grml` config
* Powerlevel 9k prompt theme
	* Left: [user@host] > ~/path/to/dir > git info > exit status >
	* Right: < Command # < Time executed
* `zsh-syntax-highlighting`
* Some useful aliases and functions
* Script to configure everything to you and clone plugins
