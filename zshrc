#
# Add zsh plugins and other feature stuff
#

# Grml config
source ${HOME}/.zsh-grml

# Set up for using colors and prompts
TERM=xterm-256color

# Add powerline -- Disable grml prompt and source powerline
prompt off
source ${HOME}/.zsh-powerlevel9k/powerlevel9k.zsh-theme
POWERLEVEL9K_MODE='awesome-fontconfig'
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(context dir vcs status)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(history time)
DEFAULT_USER="joe"
DEFAULT_HOST="r9"

# Make up-down keys search history for the whole line so far
# instead of just the first word on the line
autoload -U history-search-end
bindkey "^[OA" history-beginning-search-backward-end
bindkey "^[OB" history-beginning-search-forward-end

#
# Add Aliases
#

# Color aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias diff='diff --color=auto'
alias ip='ip -color=auto'
alias minicom='minicom -c on'
alias gitl='git log --graph --all --decorate'

# ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Helpful commands
alias pacman='sudo pacman'

# Clean up dangling docker images
alias docker_rmi_dangling='docker rmi $(docker images -f "dangling=true" -q)'
alias clipboard='xclip -selection c'

# Pipe program output into X clipboard
alias clipboard='xclip -selection c'

#
# Useful functions
#

# Highlight the passed word in stdout
highlight () {
	perl -pe "s/$1/\e[1;31m$&\e[0m/g"
}


# Dump a file in the hex format that sqlite3 wants
function sqlite3_hexdump() {
	hexdump -ve '1/1 "%.2x"' $@
}

#
# Set up environment variables
#

# Set editor to vim
export EDITOR="vim"
# Add my bin dir to PATH
export "PATH=$PATH:${HOME}/.local/bin"


# Activate zsh-syntax-highlighting
source ${HOME}/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
