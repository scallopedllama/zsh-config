#/bin/sh

STARTDIR="$(pwd)"

if [ ! -e "setup_zsh.sh" ]; then
    echo "Please run this script from the setup_zsh git root"
    exit 1
fi

# Pull components

# Figure out if it should use wget or curl
CURL="curl"
if ! which curl > /dev/null; then
        CURL="wget -q -O-"
        if ! which wget > /dev/null; then
                echo "No tool to pull files from the internet installed."
                echo "Install wget or curl and try again"
                exit 1
        fi
fi

# Make sure zsh is installed
if ! which zsh > /dev/null; then
	echo "zsh not installed"
	echo "This script just steals Joe's zsh config, It can't install zsh for you."
	echo "Install zsh with your package manager and try again."
	exit 1
fi

# zsh grml config
echo "Pulling zsh-grml config..."
GRML_PATH="${HOME}/.zsh-grml"
rm -rf "${GRML_PATH}" 2> /dev/null
"${CURL}" 'http://git.grml.org/?p=grml-etc-core.git;a=blob_plain;f=etc/zsh/zshrc;hb=HEAD' > ${GRML_PATH}

# Powerlevel 9k
echo ""
echo "Pulling Powerlevel 9k Prompt Theme..."
POWERLEVEL_PATH="${HOME}/.zsh-powerlevel9k"
rm -rf "${POWERLEVEL_PATH}" 2> /dev/null
git clone https://github.com/bhilburn/powerlevel9k.git "$POWERLEVEL_PATH"

# Syntax Highlighting
echo ""
echo "Pulling zsh-syntax-highlighting..."
SYNTAX_PATH="${HOME}/.zsh-syntax-highlighting"
rm -rf "${SYNTAX_PATH}" 2> /dev/null
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git "$SYNTAX_PATH"

# My zshrc
echo ""
echo "Shamelessly stealing Joe's zshrc config file..."
cp "${STARTDIR}/zshrc" "${HOME}/.zshrc"

# Update .zshrc paths

echo ""
echo "Fixing minor details in zshrc..."

# Change default user to $USER, host to $(hostname)
sed -i 's|^.*DEFAULT_USER=.*$|DEFAULT_USER="'"${USER}"'"|g' "${HOME}/.zshrc"
sed -i 's|^.*DEFAULT_HOST=.*$|DEFAULT_HOST="'"$(hostname)"'"|g' "${HOME}/.zshrc"

# Should be good
echo -e "\n\n\n"
echo "Your zsh config is all set up!"
echo "Run the following to change your shell to zsh starting your next login:"
echo '    chsh -s $(which zsh) ${USER}'
echo "Or just run zsh to try it out!"
echo ""
echo "Note: You must have the powerline-patched fonts installed on your host desktop computer"
echo "      (not the computer into which you have SSH'd) or the prompt won't look so nice."
echo "      These are provided by powerline-fonts on ArchLinux, or you can pull one of the"
echo "      fonts from the repository here: https://github.com/powerline/fonts and install"
echo "      it manually, then configure your terminal to use it (this is how you'd have to do it for Windows)."
echo ""
echo "This setup assumes that this computer is your main one. The user@host part of the prompt"
echo "is only displayed if your user and / or hostname do not match the values set in the .zshrc"
echo "variables DEFAULT_USER and DEFAULT_HOST. This is so that you don't see that part when working"
echo "on your computer, but they're displayed when you ssh somewhere else. Update those variables"
echo "to your liking."
echo ""
echo "Enjoy!"

